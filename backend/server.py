from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
from flask_jsonpify import jsonify
from flask_cors import CORS
import json

from sympy.codegen.fnodes import use

app = Flask(__name__)
CORS(app)
api = Api(app)


class Comercio:

    def __init__(self, id, name, direccion, tipoComercio):
        self.id = id
        self.name = name
        self.direccion = direccion
        self.tipoComercio = tipoComercio

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'direccion': self.direccion,
            'tipoComercio': self.tipoComercio,
        }


com1 = Comercio(0, 'Simago', 'Preciados 12', 'Supermercado');
com2 = Comercio(1, 'Juana Torres', 'Comercio 23', 'Farmacia');
coms = []
coms.append(com1)
coms.append(com2)


class Voluntario:

    def __init__(self, id, name, surname, distance):
        self.id = id
        self.name = name
        self.surname = surname
        self.distance = distance

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'surname': self.surname,
            'distance': self.distance,
        }


vol1 = Voluntario(0, 'Pablo', 'Perez', 200);
vol2 = Voluntario(1, 'Irene', 'Garcia', 200);

vols = []
vols.append(vol1)
vols.append(vol2)


class Confinado:

    def __init__(self, id, name, surname, direccion, long, lat):
        self.id = id
        self.name = name
        self.surname = surname
        self.direccion = direccion
        self.long = long
        self.lat = lat

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'surname': self.surname,
            'direccion': self.direccion,
            'long': self.long,
            'lat': self.lat

        }


conf1 = Confinado(0, 'Alicia', 'Ruiz', 'Paseo Castellana', '2.043452386291875', '41.380009481103656');
conf2 = Confinado(1, 'Paco', 'Porras', 'Ronda Nelle', '2.043452386291875', '41.37961501551621');

confs = []
confs.append(conf1)
confs.append(conf2)


class Test(Resource):
    def get(self):
        return 'test'


class ListVoluntarios(Resource):
    def get(self):
        return jsonify([e.serialize() for e in vols])
        # return jsonify(users)


class DetailVoluntarios(Resource):
    def get(self, id_detail):
        return jsonify(vols[int(id_detail)].__dict__)


class ListComercios(Resource):
    def get(self):
        return jsonify([e.serialize() for e in coms])


class DetailComercios(Resource):
    def get(self, id_detail):
        return jsonify(coms[int(id_detail)].__dict__)


class ListConfinados(Resource):
    def get(self):
        return jsonify([e.serialize() for e in confs])


class DetailConfinados(Resource):
    def get(self, id_detail):
        return jsonify(confs[int(id_detail)].__dict__)


api.add_resource(Test, '/test')  # Route_1

api.add_resource(ListVoluntarios, '/listVoluntarios')  # Route_2

api.add_resource(DetailVoluntarios, '/detailVoluntarios/<id_detail>')  # Route_3

api.add_resource(ListComercios, '/listComercios')  # Route_4

api.add_resource(DetailComercios, '/detailComercios/<id_detail>')  # Route_5

api.add_resource(ListConfinados, '/listConfinados')  # Route_6

api.add_resource(DetailConfinados, '/detailConfinados/<id_detail>')  # Route_7

if __name__ == '__main__':
    app.run(port='5002')
