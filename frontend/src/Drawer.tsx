import { Avatar, Button, Card, CardContent, Chip, Grid, makeStyles, Paper, Typography, useTheme } from '@material-ui/core';
import React from 'react';
import drawer_1 from './images/drawer_1.svg'
import drawer_2 from './images/drawer_2.svg'

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3)
  },
  title_main: {
    fontWeight: 'bold',
    color: theme.palette.primary.dark
  },
  title_secondary: {
    color: theme.palette.primary.light
  },
  card: {
    borderRadius: 20,
    minHeight: '150px'
  },
  img_wrapper: {
    overflow: 'hidden',
    backgroundColor: theme.palette.primary.light
  },
  img: {
    transform: 'scale(6) translate(32px, -20px)',
    width: '100%',
    height: '100%'
  },
  card_content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  card_button: {
    paddingLeft: '30px',
    paddingRight: '30px',
    borderRadius: 10
  }
}))

const mockTarea = (user:string, distance:number, title?: string, description?:string) => {
  return {
    user,
    distance,
    title,
    description
  }
}

const Chips = () => {
  const theme = useTheme();
  return (
    <div style={{
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(4)
    }}>
      <Chip
        avatar={<Avatar>M</Avatar>}
        label="Menos de 300m"
        color="primary"
        variant="outlined"
        component={Paper}
        elevation={3}
      />
    </div>
  )
}
const Drawer = ({ confinados }) => {
  const classes = useStyles();
  const mockTareas = [
    mockTarea('Dani Rey', 10),
    mockTarea('Andrea Ruiz Coll', 310),
    mockTarea('Daniil Teterevkov', 15)
  ];

  return (
    <div className={classes.root}>
      <Typography variant="h4">
        <span className={classes.title_main}>7 Resultados</span>
        <span className={classes.title_secondary}> en tu zona</span>
      </Typography>
      <Typography variant="h6">300m</Typography>
      <Chips />
      <Grid container spacing={3}>
      {/* {mockTareas.map((tarea, index) => {
        return (
          <Grid item xs={6}>  
            <Card className={classes.card} elevation={3}>
              <Grid container direction="row" style={{ minHeight: 'inherit' }}>
                <Grid item xs={4} className={classes.img_wrapper}>
                  <img className={classes.img} src={index % 2 == 0 ? empty_cart : healthy_options} />
                </Grid>
                <Grid item xs={8} className={classes.card_content}>
                  <Typography style={{ textAlign: 'center', fontWeight: 'bold' }} variant="h6">{tarea.user.toUpperCase()}</Typography>
                  <Typography variant="subtitle1" style={{ marginTop: '-5px', fontWeight: 'bold' }}>{`${tarea.distance} m`}</Typography>
                  <Button variant="outlined" color="secondary" className={classes.card_button}>
                    <span style={{ fontWeight: 'bold'}}>
                      Aceptar
                    </span>
                  </Button>
                </Grid>
              </Grid>
            </Card>
          </Grid>
          )
      })} */}
      {Array.isArray(confinados) && confinados.map((user, index) => {
        return (
          <Grid item xs={6}>  
            <Card className={classes.card} elevation={3}>
              <Grid container direction="row" style={{ minHeight: 'inherit' }}>
                <Grid item xs={4} className={classes.img_wrapper}>
                  <img className={classes.img} src={drawer_1} />
                </Grid>
                <Grid item xs={8} className={classes.card_content}>
                  <Typography style={{ textAlign: 'center', fontWeight: 'bold' }} variant="h6">{`${user.name} ${user.surname}`.toUpperCase()}</Typography>
                  <Typography variant="subtitle1" style={{ marginTop: '-5px', fontWeight: 'bold' }}>{`50 m`}</Typography>
                  <Button variant="outlined" color="secondary" className={classes.card_button}>
                    <span style={{ fontWeight: 'bold'}}>
                      Aceptar
                    </span>
                  </Button>
                </Grid>
              </Grid>
            </Card>
          </Grid>
          )
      })}
      </Grid>
    </div>
  );
};

export default Drawer;
