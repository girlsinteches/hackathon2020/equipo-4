import * as React from 'react';
import splashImg from './images/splash.svg'

const Splash = () => {
  return (
    <img src={splashImg} />
  )
}

export default Splash;