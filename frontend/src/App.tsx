import React from 'react';
import { useQuery } from 'react-query';
import MapDrawer from './MapDrawer';
import AppBar from './AppBar';
import Splash from './Splash';
import { makeStyles, CssBaseline } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';

const fetchPlanets = async () => {
  const res = await fetch('https://swapi.dev/api/planets/');
  return res.json();
}

const useStyles = makeStyles({
  root: {
    height: '100vh',
    width: '100vw',
    overflowX: 'hidden'
  }
});

function App() {
  const { root } = useStyles();
  const { data, status } = useQuery('planets', fetchPlanets);
  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <div className={root}>
        <Splash />
        <CssBaseline />
        <AppBar />
        <MapDrawer />
        {/* {status === 'loading' && (
          <div>Loading data...</div>
        )}
        {status === 'error' && (
          <div>Error fetching data</div>
        )}
        {status === 'success' && data.results.map((planet) => {
          console.log(planet);
          return null;
        })} */}
      </div>
    </MuiPickersUtilsProvider>
  );
}

export default App;
