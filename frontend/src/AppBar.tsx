import * as React from 'react';
import { AppBar as MuiAppBar, Button, Grid, Toolbar, Typography } from '@material-ui/core';
import logo from './images/logo.svg'

const AppBar = () => {
    const navItems = ['Inicio', 'Mapa', 'Faqs', 'Registro'];

    const orangecolor = '#FF8D3E';

    return (
        <MuiAppBar elevation={0} color="transparent">
            <Toolbar component={Grid} container>
                <img src={logo} style={{ paddingTop: '10px' }} />
                <div style={{ flexGrow: 1 }} />
                    {navItems.map((item) => (
                        <Grid item style={{ paddingRight: '100px' }}>
                            <Typography variant="h5" style={{ color: orangecolor, fontWeight: 'bold' }}>{item}</Typography>
                        </Grid>
                    ))}
                {/* TODO: put content here */}
            </Toolbar>
        </MuiAppBar>
    );
}

export default AppBar;