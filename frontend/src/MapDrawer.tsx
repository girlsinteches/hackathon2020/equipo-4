import { Grid, makeStyles, Paper, Theme } from '@material-ui/core';
import * as React from 'react';
import Map from './Map';
import Drawer from './Drawer';
import Filters from './Filters';
import { useQuery } from 'react-query';

const useStyles = makeStyles((theme:Theme) => ({
  root: {
    height: 'inherit',
  },
  toolbar: theme.mixins.toolbar,
  paper: {
    zIndex: 10
  }
}));

const MapDrawer = () => {
  const classes = useStyles();
  const { data : confinados , status } = useQuery('confinados', async () => {
    const list = await fetch ('http://127.0.0.1:5002/listConfinados')
    return list.json();
  })
  // const { data : detail, status: statusDetail } = useQuery('detail', async () => {
  //   const detail = await fetch('http://127.0.0.1:5002/detail/1');
  //   return detail.json();
  // })
  return (
    <>
      <div className={classes.toolbar} />
      <Grid container className={classes.root}>
        <Grid item xs={8} style={{ position: 'relative' }}>
          <Filters />
          <Map confinados={status === "success" ? confinados : []} />
        </Grid>
        <Grid item xs={4} elevation={10} component={Paper} className={classes.paper}>
          <Drawer confinados={confinados}/>
        </Grid>
      </Grid>
    </>
  )
}

export default MapDrawer;